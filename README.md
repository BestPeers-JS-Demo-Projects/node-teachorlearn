

To install the dependencies, run this in the application folder from the command-line:

```bash
$ npm install
```

## Running Your Application

Run your application using npm:

```bash
$ npm start
```

Your application should run on port 3000 with the *development* environment configuration, so in your browser just go to [http://localhost:3000](http://localhost:3000)

