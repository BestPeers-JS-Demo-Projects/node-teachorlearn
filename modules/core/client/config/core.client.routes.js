(function () {
  'use strict';

  angular
    .module('core.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

  function routeConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.rule(function ($injector, $location) {
      var path = $location.path();
      var hasTrailingSlash = path.length > 1 && path[path.length - 1] === '/';

      if (hasTrailingSlash) {
        // if last character is a slash, return the same url without the slash
        var newPath = path.substr(0, path.length - 1);
        $location.replace().path(newPath);
      }
    });

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: '/modules/core/client/views/home.client.view.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state('not-found', {
        url: '/not-found',
        templateUrl: '/modules/core/client/views/404.client.view.html',
        controller: 'ErrorController',
        controllerAs: 'vm',
        params: {
          message: function ($stateParams) {
            return $stateParams.message;
          }
        },
        data: {
          ignoreState: true
        }
      })
      .state('bad-request', {
        url: '/bad-request',
        templateUrl: '/modules/core/client/views/400.client.view.html',
        controller: 'ErrorController',
        controllerAs: 'vm',
        params: {
          message: function ($stateParams) {
            return $stateParams.message;
          }
        },
        data: {
          ignoreState: true
        }
      })
      .state('about', {
        abstract: true,
        url: '/about-us',
        templateUrl: '/modules/core/client/views/about-us/about-main.client.view.html',
      })
      .state('about.aboutus', {
        url: '/who-we-are',
        templateUrl: '/modules/core/client/views/about-us/aboutus.client.view.html',
        data: {
          pageTitle: 'About Us'
        }
      })
      .state('about.reviews', {
        url: '/reviews',
        templateUrl: '/modules/core/client/views/about-us/reviews.about-us.client.view.html',
        data: {
          pageTitle: 'Reviews'
        }
      })
      .state('about.team', {
        url: '/team',
        templateUrl: '/modules/core/client/views/about-us/team.about-us.client.view.html',
        data: {
          pageTitle: 'Our Team'
        }
      })
      .state('about.partners', {
        url: '/partners',
        templateUrl: '/modules/core/client/views/about-us/partners.about-us.client.view.html',
        data: {
          pageTitle: 'Our Partners'
        }
      })
      .state('about.volunteer', {
        url: '/volunteer',
        templateUrl: '/modules/core/client/views/about-us/volunteer.about-us.client.view.html',
        data: {
          pageTitle: 'Volunteer'
        }
      })
      .state('about.donate', {
        url: '/donate',
        templateUrl: '/modules/core/client/views/about-us/donate.about-us.client.view.html',
        data: {
          pageTitle: 'Donate Us'
        }
      })
      .state('about.blog', {
        url: '/blog',
        templateUrl: '/modules/core/client/views/about-us/blog.about-us.client.view.html',
        data: {
          pageTitle: 'Blog'
        }
      })
      .state('about.help', {
        url: '/help',
        templateUrl: '/modules/core/client/views/about-us/help.about-us.client.view.html',
        data: {
          pageTitle: 'Help Desk'
        }
      })
      .state('about.contact', {
        url: '/contact',
        templateUrl: '/modules/core/client/views/about-us/contact.about-us.client.view.html',
        data: {
          pageTitle: 'Help Desk'
        }
      })
      .state('forbidden', {
        url: '/forbidden',
        templateUrl: '/modules/core/client/views/403.client.view.html',
        data: {
          ignoreState: true
        }
      });
  }
}());
