(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', 'Authentication', 'menuService', '$timeout', 'DialogModal'];

  function HeaderController($scope, $state, Authentication, menuService, $timeout, DialogModal) {
    var vm = this;

    vm.accountMenu = menuService.getMenu('account').items[0];
    vm.authentication = Authentication;
    vm.isCollapsed = false;
    vm.menu = menuService.getMenu('topbar');

    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      // Collapsing the menu after navigation
      vm.isCollapsed = false;
    }

    // ng-dialog role-signup-modal from header signup button

    vm.clickToOpen = DialogModal.clickToOpen ;

    // ng-dialog role-signup-modal from header signup button

  vm.openSignInModal = DialogModal.openSignInModal ;

  }
}());
