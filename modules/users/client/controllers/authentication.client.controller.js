(function () {
  'use strict';

  angular
    .module('users')
    .controller('AuthenticationController', AuthenticationController);

  AuthenticationController.$inject = ['$scope', '$state', 'UsersService', '$location', '$window', 'Authentication', 'PasswordValidator', 'Notification', 'ngDialog', 'DialogModal'];

  function AuthenticationController($scope, $state, UsersService, $location, $window, Authentication, PasswordValidator, Notification, ngDialog, DialogModal) {
    var vm = this;

    vm.authentication = Authentication;
    vm.getPopoverMsg = PasswordValidator.getPopoverMsg;
    vm.signup = signup;
    vm.signin = signin;
    vm.assignRole = assignRole ;
    vm.roles = [] ;
    vm.callOauthProvider = callOauthProvider;
    vm.openSignUpModal = openSignUpModal ;
    vm.clickToOpen = DialogModal.clickToOpen ;
    vm.openSignInModal = DialogModal.openSignInModal ;
    
    vm.usernameRegex = /^(?=[\w.-]+$)(?!.*[._-]{2})(?!\.)(?!.*\.$).{3,34}$/;

    // Get an eventual error defined in the URL query string:
    if ($location.search().err) {
      Notification.error({ message: $location.search().err });
    }

    // If user is signed in then redirect back home
    if (vm.authentication.user) {
      $location.path('/');
    }

    //role assign from dialog 1
    

     function assignRole (roleVal){
      vm.roles.push(roleVal) ;

      vm.openSignUpModal();

  };

  function openSignUpModal(){

     var windowIDs = ngDialog.getOpenDialogs();
     ngDialog.close(windowIDs[0]);

    //ng dialogue modal for sign-up

       vm.dialog2 = ngDialog.open({ template: '/modules/users/client/views/authentication/signup.client.view.html',
         className: 'ngdialog-theme-default',
         data: {
            roles : vm.roles
            },
        closeByNavigation : true
          });

       vm.dialog2Id = vm.dialog2.id ;
  }


    function signup(isValid) {

      vm.credentials.roles = $scope.ngDialogData.roles ;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');

        return false;
      }

      UsersService.userSignup(vm.credentials)
        .then(onUserSignupSuccess)
        .catch(onUserSignupError);
    }

    function signin(isValid) {

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.userForm');

        return false;
      }

      UsersService.userSignin(vm.credentials)
        .then(onUserSigninSuccess)
        .catch(onUserSigninError);
    }

    // OAuth provider request
    function callOauthProvider(url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    }

    // Authentication Callbacks

    function onUserSignupSuccess(response) {

      // ngDialog.close(ngdialog2);
      var openModalId = ngDialog.getOpenDialogs();
      ngDialog.close(openModalId[0]);

      // If successful we assign the response to the global user model
      // vm.authentication.user = response;
      Notification.info({ message: 'Welcome , verification email has been sent. please verify to register with us !! ' });
      // And redirect to the previous or home page
      $state.go('authentication.confirmemail');
    }

    function onUserSignupError(response) {
      Notification.error({ message: response.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Signup Error!', delay: 6000 });
    }

    function onUserSigninSuccess(response) {
      //close sign in modal

      var openModalId = ngDialog.getOpenDialogs();
      ngDialog.close(openModalId[0]);

      // If successful we assign the response to the global user model
      vm.authentication.user = response;
      Notification.info({ message: 'Welcome ' + response.firstName });
      // And redirect to the previous or home page
      $state.go($state.previous.state.name || 'home', $state.previous.params);
    }

    function onUserSigninError(response) {
      if(response.data.type == 'not-verified'){
        $state.go('emailnotverified');
      }
      else{
        Notification.error({ message: response.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Signin Error!', delay: 6000 });
      }
    }

  }
}());
