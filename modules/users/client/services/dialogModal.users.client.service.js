(function () {
  'use strict';

  // PasswordValidator service used for testing the password strength
  angular
    .module('users.services')
    .factory('DialogModal', DialogModal);

  DialogModal.$inject = ['$window', 'ngDialog'];

  function DialogModal($window, ngDialog) {

    var service = {
      clickToOpen: clickToOpen,
      openSignInModal: openSignInModal
    };

    return service;

    // open rrole-signup-modal

  function clickToOpen() {

      var currentModalId = ngDialog.getOpenDialogs();
      ngDialog.close(currentModalId[0]);


    ngDialog.open({ template: '/modules/users/client/views/authentication/authentication.role-Modal.users.client.view.html',
         className: 'ngdialog-theme-default',
           preserveFocus: false,
           closeByNavigation : true
     });

    };

    // open sign in modal ng-dilaog
    function openSignInModal(){
      var currentModalId = ngDialog.getOpenDialogs();
      ngDialog.close(currentModalId[0]);

      /* open sign in modal*/

      ngDialog.open({ template: '/modules/users/client/views/authentication/signin.client.view.html',
         className: 'ngdialog-theme-default',
        closeByNavigation : true
          });



    }

  }

}());