
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Comment Schema
 */
var TokenSchema = new Schema({
  _userId: { 
      type: Schema.ObjectId,
      required: true,
      ref: 'User' 
    },
  token: { 
      type: String,
      required: true
    },
  createdAt: { 
      type: Date,
      required: true,
      default: Date.now,
      expires: 43200 }

});

mongoose.model('Token', TokenSchema);