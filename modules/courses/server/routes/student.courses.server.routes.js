'use strict';

/**
 * Module dependencies
 */

 var student = require('../controllers/student.courses.server.controller');
 var courses = require('../controllers/courses.server.controller');

module.exports = function(app) {
  // student Routes
  app.route('/api/students')
    .get(student.list)
    .post(student.create);

  app.route('/api/students/:studentId')
    .get(student.read)
    .put(student.update)
    .delete(student.delete);

  // Finish by binding the student middleware
  app.param('studentId', student.studentByID);
};
