'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Course Schema
 */
var CourseSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Course name',
    trim: true
  },
  content:[{
    content_url: {
    type: String  
  },
  content_type: {
    type: String
  },
  content_name: {
    type: String
  },
  content_uploaded: {
    type: Date,
    default: Date.now
  }  
  }],
  created: {
    type: Date,
    default: Date.now
  },
  owner: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  language: {
    type: String
  },
  category:{
    main_category: {
        type: String
    },
    sub_category: {
      type: String
    }
  },
  difficulty_level: {
    type: String
  },
  experience_level: {
    type: String
  },
  education_level: {
    type: String
  },
  media_type: {
    type: String
  },
  media_skills: {
    type: String
  },
  accreditation: {
    type: String
  },
  description:{
    type: String
  },
  objective:{
    type: String
  },
  students:[{
    type:Schema.ObjectId,
    ref: 'Student'
  }],
  rating:{
    type: Number,
    default: 0
  }

});

mongoose.model('Course', CourseSchema);
