
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Comment Schema
 */
var StudentSchema = new Schema({
  student: { 
      type: Schema.ObjectId,
      required: true,
      ref: 'User' 
    },
  tasks: { 
      type: String
    },
  created: { 
      type: Date,
      default: Date.now
    },
    course: {
      type: Schema.ObjectId,
      ref: 'Course'
    },
  grade:{
    type: String
  }

});

mongoose.model('Student', StudentSchema);