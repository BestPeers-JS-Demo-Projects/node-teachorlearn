'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  multer = require('multer'),
  config = require(path.resolve('./config/config')),
  Course = mongoose.model('Course'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Course
 */
exports.create = function(req, res) {
  console.log(req.body);

  var course = new Course(req.body);
  course.owner = req.user;

  course.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(course);
    }
  });
};

/**
 * Show the current Course
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var course = req.course ? req.course.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  course.isCurrentUserOwner = req.user && course.owner && course.owner._id.toString() === req.user._id.toString();

  res.jsonp(course);
};

/**
 * Update a Course
 */
exports.update = function(req, res) {
  var course = req.course;

  course = _.extend(course, req.body);

  course.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(course);
    }
  });
};

/**
 * Delete an Course
 */
exports.delete = function(req, res) {
  var course = req.course;

  course.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(course);
    }
  });
};

/**
 * List of Courses
 */
exports.list = function(req, res) {
  Course.find(req.query).sort('-created').populate('owner', 'displayName').exec(function(err, courses) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(courses);
    }
  });
};


/* Seacrh in Courses*/

exports.search = function(req, res) {
  console.log('called search') ;
  console.log(req.query);

  var key = req.query.key ;
  console.log(key);
  var value = req.query.value ;

  Course.find().where(key).equals(value).sort('-created').populate('owner', 'displayName profileImageURL').exec(function(err, courses) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(courses);
    }
  });
};


//course content upload

exports.courseContentUpload = function(req, res) {
 
console.log('called') ;
var course = req.course ;
var upload = multer( { dest: './modules/courses/client/uploaded-files/course-content/' } ).array('files') ;

 upload(req, res, function () {
  console.log(req.files);
        for(var i =0; i < req.files.length ; i++){
        course.content.push({
        content_url : req.files[i].destination + req.files[i].filename ,
        content_type: req.files[i].mimetype,
        content_name: req.files[i].originalname
        });
    course.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.status(200).send('success');
    }
  });
  }

 });
  

};

/**
 * Course middleware
 */
exports.courseByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Course is invalid'
    });
  }

  Course.findById(id).populate('owner', 'displayName profileImageURL').exec(function (err, course) {
    if (err) {
      return next(err);
    } else if (!course) {
      return res.status(404).send({
        message: 'No Course with that identifier has been found'
      });
    }
    req.course = course;
    next();
  });
};
