'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Courses Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/courses',
      permissions: '*'
    }, {
      resources: '/api/courses/:courseId',
      permissions: '*'
    },
    {
      resources: '/api/courses/search',
      permissions: '*'
    },
    ,
     {
      resources: '/api/courses/:courseId/uploadContent',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/courses',
      permissions: ['get', 'post']
    }, {
      resources: '/api/courses/:courseId',
      permissions: ['get']
    },
    {
      resources: '/api/courses/search',
      permissions: ['get']
    },
    ,
     {
      resources: '/api/courses/:courseId/uploadContent',
      permissions: ['get','post']
    }]
  },
  {
    roles: ['educator'],
    allows: [{
      resources: '/api/courses',
      permissions: ['get', 'post']
    }, {
      resources: '/api/courses/:courseId',
      permissions: ['get', 'put', 'delete']
    },
    {
      resources: '/api/courses/search',
      permissions: ['get']
    },
     {
      resources: '/api/courses/:courseId/uploadContent',
      permissions: ['get','post']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/courses',
      permissions: ['get']
    }, {
      resources: '/api/courses/:courseId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Courses Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an Course is being processed and the current user created it then allow any manipulation
  if (req.course && req.user && req.course.user && req.course.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
