(function () {
  'use strict';

  // Courses controller
  angular
    .module('courses')
    .controller('CoursesController', CoursesController);

  CoursesController.$inject = ['$scope', '$state', '$window', 'Authentication', 'courseResolve', 'CategoryService', 'Notification', 'studentResolve','$stateParams'];

  function CoursesController ($scope, $state, $window, Authentication, course, CategoryService, Notification, student, $stateParams) {
    var vm = this;

    vm.authentication = Authentication;
    vm.course = course;
    vm.student = student ;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.joinClass = joinClass ;
    vm.category = CategoryService ;

    console.log($stateParams);
    console.log(vm.course._id);

    //education level array 

    vm.educationLevel = [
    {'education_level': 'POST GRADUATE'},
    {'education_level': 'UNDER GRADUATE'},
    {'education_level': 'HIGH SCHOOL'},
    {'education_level': 'MIDDLE SCHOOL'},
    {'education_level': 'ELEMENTRY SCHOOL'},
    {'education_level': 'KINDERGARTEN'},
    {'education_level': 'PRE-KINDERGARTEN'},
    ];

    // difficulty level array

    vm.difficultyLevel = [
    {'difficulty_level': 'ADVANCED'},
    {'difficulty_level': 'INTERMEDIATE'},
    {'difficulty_level': 'BEGINNER'}
    ]

    //language array

   vm.lang = [
   {'language' : 'English'},
   {'language' : 'Spanish'},
   {'language' : 'French'},
   {'language' : 'Italian'},
   {'language' : 'Hindi'}
   ] ;

   // experience level

   vm.experienceLevel = [
   {'experience_level': '0-1'},
   {'experience_level': '1-3'},
   {'experience_level': '3-5'},
   {'experience_level': '5-10'},
   {'experience_level': '10-15'},
   {'experience_level': '15-20'},
   {'experience_level': '20-30'}
   ];

   // media type

   vm.mediaType = [
   {'media_type': 'Audio'},
   {'media_type': 'Video'},
   {'media_type': 'PDF'},
   {'media_type': 'TEXT'},
   {'media_type': 'PPT'},
   {'media_type': 'All'}
   ];

   // Media Skills

   vm.mediaSkills = [
   {'media_skills': 'only AUDIO'},
   {'media_skills': 'only VIDEO'},
   {'media_skills': 'only PPT'},
   {'media_skills': 'only TEXT'},
   {'media_skills': 'only PDF'},
   {'media_skills': 'All'}
   ] ;

    // Remove existing Course
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.course.$remove($state.go('courses.list'));
      }
    }

    // Save Course
    function save(isValid) {

          vm.course.category = {} ;
        vm.course.category.main_category = vm.selected_category.name ;
        vm.course.category.sub_category = vm.selected_sub_category ;
        vm.course.education_level = vm.selected_education_level.education_level ;
        vm.course.difficulty_level = vm.selected_difficulty_level.difficulty_level ;
        vm.course.language = vm.selected_language.language ;
        vm.course.experience_level = vm.selected_experience_level.experience_level ;
        vm.course.media_type = vm.selected_media_type.media_type ;
        vm.course.media_skills = vm.selected_media_skills.media_skills ;


      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.courseForm');
        return false;
      }

       console.log(vm.course) ;

      // TODO: move create/update logic to service
      if (vm.course._id) {
        vm.course.$update(successCallback, errorCallback);
      } else {
        vm.course.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        console.log(res._id);
        $state.go('courseSetting.objective', {
          courseId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function joinClass(){
      vm.student.$save(successCallback, errorCallback);

           function successCallback(res) {
        console.log(res);
        // $state.go('courseSetting.objective', {
        //   courseId: res._id
        // });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    };
  }
}());
