(function () {
  'use strict';

  angular
    .module('courses')
    .controller('CoursesSettingController', CoursesSettingController);

  CoursesSettingController.$inject = ['$scope','CoursesService', 'courseResolve', '$stateParams', 'Notification'];

  function CoursesSettingController($scope, CoursesService, course, $stateParams, Notification) {
    var vm = this;

    vm.course = course;
    vm.save = save;

    console.log($stateParams);

      //Set options for dropzone
  //Visit http://www.dropzonejs.com/#configuration-options for more options
  $scope.dzOptions = {
    url : '/api/courses/'+$stateParams.courseId +'/uploadContent',
    paramName : 'files',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true
  };
  
  
  //Handle events for dropzone
  //Visit http://www.dropzonejs.com/#events for more events
  $scope.dzCallbacks = {
    'addedfile' : function(file){
      console.log(file);
      $scope.newFile = file;
    },
    'success' : function(file, res){
      console.log(res.msg);
    }
  };
  
  
  //Apply methods for dropzone
  //Visit http://www.dropzonejs.com/#dropzone-methods for more methods
  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
  }

    // Save Course
    function save(isValid) {


      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.courseForm');
        return false;
      }

       console.log(vm.course) ;

      // TODO: move create/update logic to service
      if (vm.course._id) {
        vm.course.$update(successCallback, errorCallback);
      } else {
        vm.course.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Course deleted successfully!' });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());

