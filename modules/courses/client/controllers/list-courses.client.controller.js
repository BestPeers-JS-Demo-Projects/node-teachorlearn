(function () {
  'use strict';

  angular
    .module('courses')
    .controller('CoursesListController', CoursesListController);

  CoursesListController.$inject = ['CoursesService', '$filter'];

  function CoursesListController(CoursesService, $filter) {
    var vm = this;
     vm.searchCategory = searchCategory ;
     vm.allCourse = allCourse ;

     vm.courses =  CoursesService.query();

     function allCourse(){
      CoursesService.query(function(data){
      vm.courses = data ;
     });
    };

    vm.search = '' ;

       vm.filteredItems = $filter('filter')(vm.courses, {
        $: vm.search
      });

       function searchCategory(sk, val){
          console.log('called') ;
          console.log(sk);
          console.log(val);
        CoursesService.search({key : sk, value: val},function(data){
          vm.courses = data ;
          console.log(data);
        });

       };


  }
}());
