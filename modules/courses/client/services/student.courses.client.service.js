// Students service used to communicate Students REST endpoints

(function () {
  'use strict';

  angular
    .module('courses.services')
    .factory('StudentService', StudentService);

  StudentService.$inject = ['$resource', '$log'];

  function StudentService($resource, $log) {
    var Student = $resource('/api/students/:studentId', {
      studentId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Student.prototype, {
      createOrUpdate: function () {
        var student = this;
        return createOrUpdate(student);
      }
    });

    return Student;

    function createOrUpdate(student) {
      if (student._id) {
        return student.$update(onSuccess, onError);
      } else {
        return student.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(student) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
