// Courses service used to communicate Courses REST endpoints

(function () {
  'use strict';

  angular
    .module('courses.services')
    .factory('CategoryService', CategoryService);

  CategoryService.$inject = ['$resource', '$log'];

  function CategoryService($resource, $log) {
    var Category = [{
      'id': 1,
      'name': 'IT',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Network and Security'
      },
      {   
        'id': 2,
        'name': 'Hardware'
      },
      {   
        'id': 3,
        'name': 'Software Development'
      },
      {   
        'id': 4,
        'name': 'Game Development'
      },
      {   
        'id': 5,
        'name': 'IT Management'
      },
      {   
        'id': 6,
        'name': 'Mobile Apps'
      },
      {   
        'id': 7,
        'name': 'Software Engineering'
      },
      {   
        'id': 8,
        'name': 'Data Science'
      },
      {   
        'id': 9,
        'name': 'Databases'
      },
      {   
        'id': 10,
        'name': 'Core IT Skills'
      }]
    },
    {
      'id': 2,
      'name': 'Business',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Human Resources'
      },
      {   
        'id': 2,
        'name': 'Operations'
      },
      {   
        'id': 3,
        'name': 'E-Commerce'
      },
      {   
        'id': 4,
        'name': 'Leadership and Management'
      },
      {   
        'id': 5,
        'name': 'Entrepreneurship'
      },
      {   
        'id': 6,
        'name': 'Project Management'
      },
      {   
        'id': 7,
        'name': 'Sales'
      },
      {   
        'id': 8,
        'name': 'Finance'
      },
      {   
        'id': 9,
        'name': 'Tourism and Hospitality'
      },
      {   
        'id': 10,
        'name': 'Communications'
      }]
    },
    {
      'id': 3,
      'name': 'Language',
      'sub_category': [
      {   
        'id': 1,
        'name': 'English'
      },
      {   
        'id': 2,
        'name': 'Spanish'
      },
      {   
        'id': 3,
        'name': 'German'
      },
      {   
        'id': 4,
        'name': 'Irish'
      },
      {   
        'id': 5,
        'name': 'French'
      },
      {   
        'id': 6,
        'name': 'Chinese'
      },
      {   
        'id': 7,
        'name': 'Arabic'
      },
      {   
        'id': 8,
        'name': 'Swedish'
      },
      {   
        'id': 9,
        'name': 'Japanese'
      }]
    },
     {
      'id': 4,
      'name': 'Science',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Life Science'
      },
      {   
        'id': 2,
        'name': 'Physics'
      },
      {   
        'id': 3,
        'name': 'General Science'
      },
      {   
        'id': 4,
        'name': 'Chemistry'
      },
      {   
        'id': 5,
        'name': 'Engineering'
      }]
    },
     {
      'id': 5,
      'name': 'Health',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Mental Health'
      },
      {   
        'id': 2,
        'name': 'Health Care'
      },
      {   
        'id': 3,
        'name': 'Nutrition'
      },
      {   
        'id': 4,
        'name': 'Fitness'
      },
      {   
        'id': 5,
        'name': 'Yoga'
      }]
    },
     {
      'id': 6,
      'name': 'Humanities',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Education'
      },
      {   
        'id': 2,
        'name': 'History'
      },
      {   
        'id': 3,
        'name': 'Politics'
      },
      {   
        'id': 4,
        'name': 'Sociology'
      },
      {   
        'id': 5,
        'name': 'Personal Development'
      },
      {   
        'id': 6,
        'name': 'Geography'
      },
      {   
        'id': 7,
        'name': 'Law'
      },
      {   
        'id': 8,
        'name': 'Psychology'
      },
      {   
        'id': 9,
        'name': 'Media and Journalism'
      },
      {   
        'id': 10,
        'name': 'Economics'
      }]
    },
     {
      'id': 7,
      'name': 'Math',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Core Maths Skills'
      },
      {   
        'id': 2,
        'name': 'Calculus'
      },
      {   
        'id': 3,
        'name': 'Probability and Statistics'
      },
      {   
        'id': 4,
        'name': 'Algebra'
      },
      {   
        'id': 5,
        'name': 'Geometry'
      },
      {   
        'id': 6,
        'name': 'Series and Sequences'
      },
      {   
        'id': 7,
        'name': 'Exam Prep'
      }]
    },
     {
      'id': 8,
      'name': 'Marketing',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Social Media Marketing'
      },
      {   
        'id': 2,
        'name': 'Marketing Management'
      },
      {   
        'id': 3,
        'name': 'Digital Marketing'
      },
      {   
        'id': 4,
        'name': 'Public Relations'
      }]
    },
    {
      'id': 9,
      'name': 'Lifestyle',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Food and Beverage'
      },
      {   
        'id': 2,
        'name': 'Art and Crafts'
      },
      {   
        'id': 3,
        'name': 'Music'
      },
      {   
        'id': 4,
        'name': 'Literature'
      },
      {   
        'id': 5,
        'name': 'Skilled Trades'
      }]
    }
    ,
    {
      'id': 10,
      'name': 'Software Development',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Front End Languages'
      },
      {   
        'id': 2,
        'name': 'Back End Languages'
      },
      {   
        'id': 3,
        'name': 'Software Testing'
      }]
    }
    ,
    {
      'id': 11,
      'name': 'Software Engineering',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Operating Systems'
      }]
    }
    ,
    {
      'id': 12,
      'name': 'Life Science',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Biology'
      },
      {   
        'id': 2,
        'name': 'Environmental Studies'
      }]
    }
    ,
    {
      'id': 13,
      'name': 'Health Care',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Nursing'
      },
      {   
        'id': 2,
        'name': 'Caregiving'
      },
      {   
        'id': 3,
        'name': 'Disease and Disorders'
      }]
    }
    ,
    {
      'id': 14,
      'name': 'Operations',
      'sub_category': [
      {   
        'id': 1,
        'name': 'Supply Chain Management'
      },
      {   
        'id': 2,
        'name': 'Business Process Management'
      },
      {   
        'id': 3,
        'name': 'Risk Management'
      },
      {   
        'id': 4,
        'name': 'Service Management'
      },
      {   
        'id': 5,
        'name': 'Management and Administration'
      },
      {   
        'id': 6,
        'name': 'Customer Service'
      },
      {   
        'id': 7,
        'name': 'Manufacturing'
      },
      {   
        'id': 8,
        'name': 'Health and Safety'
      },
      {   
        'id': 9,
        'name': 'Quality Control'
      },
      {   
        'id': 10,
        'name': 'Workplace Supervision'
      }]
    },

    ] ;

    return Category;



  }
}());
