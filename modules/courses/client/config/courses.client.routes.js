(function () {
  'use strict';

  angular
    .module('courses.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('courses', {
        abstract: true,
        url: '/courses',
        template: '<ui-view/>'
      })
      .state('courses.list', {
        url: '',
        templateUrl: '/modules/courses/client/views/list-courses.client.view.html',
        controller: 'CoursesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Courses List'
        }
      })
      .state('courses.edit', {
        url: '/:courseId/edit',
        templateUrl: '/modules/courses/client/views/form-course.client.view.html',
        controller: 'CoursesController',
        controllerAs: 'vm',
        resolve: {
          courseResolve: getCourse
        },
        data: {
          pageTitle: 'Edit Course {{ courseResolve.name }}'
        }
      })
      .state('courses.view', {
        abstract: true,
        url: '/:courseId',
        templateUrl: '/modules/courses/client/views/course-view/main.course.client.view.html',
        controller: 'CoursesController',
        controllerAs: 'vm',
        resolve: {
          courseResolve: getCourse,
          studentResolve: newStudent
        },
        data: {
          pageTitle: 'Course {{ courseResolve.name }}'
        }
      })
      .state('courses.view.about', {
        url: '/about',
        templateUrl: '/modules/courses/client/views/course-view/about.course.client.view.html',
        controller: 'CoursesController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Course {{ courseResolve.name }}'
        }
      })
      .state('courses.view.content', {
      url: '/content',
      templateUrl: '/modules/courses/client/views/course-view/content.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      })
      .state('courses.view.calender', {
      url: '/calender',
      templateUrl: '/modules/courses/client/views/course-view/calender.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      })
      .state('courses.view.discussion', {
      url: '/discussion',
      templateUrl: '/modules/courses/client/views/course-view/discussion.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      })
      .state('courses.view.grades', {
      url: '/grades',
      templateUrl: '/modules/courses/client/views/course-view/grades.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      })
      .state('courses.view.assignments', {
      url: '/assignments',
      templateUrl: '/modules/courses/client/views/course-view/assignments.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      }) 
      .state('courses.view.tutor', {
      url: '/tutor',
      templateUrl: '/modules/courses/client/views/course-view/tutor.course.client.view.html',
      controller: 'CoursesController',
      controllerAs: 'vm',
      data: {
      pageTitle: 'Course {{ courseResolve.name }}'
      }
      })   
      .state('courses.create', {
        url: '/create',
        templateUrl: '/modules/courses/client/views/class-form/form-main.course.client.view.html',
        controller: 'CoursesController',
        controllerAs: 'vm',
        resolve: {
          courseResolve: newCourse
        },
        data: {
          pageTitle: 'Courses Create'
        }
      })
      .state('courses.create.form1', {
        url: '/form1',
        templateUrl: '/modules/courses/client/views/class-form/form1.course.client.view.html'
      })
      .state('courses.create.form2', {
        url: '/form2',
        templateUrl: '/modules/courses/client/views/class-form/form2.course.client.view.html'
      })
      .state('courses.create.form3', {
        url: '/form3',
        templateUrl: '/modules/courses/client/views/class-form/form3.course.client.view.html'
      })
      .state('courses.create.form4', {
      url: '/form4',
      templateUrl: '/modules/courses/client/views/class-form/form4.course.client.view.html'
      })
      .state('courses.create.form5', {
      url: '/form5',
      templateUrl: '/modules/courses/client/views/class-form/form5.course.client.view.html'
      })
      .state('courses.create.form6', {
      url: '/form6',
      templateUrl: '/modules/courses/client/views/class-form/form6.course.client.view.html'
      })
      .state('courseSetting', {
      abstract: true,  
      url: '/:courseId/course-setting',
      templateUrl: '/modules/courses/client/views/course-setting/main.setting.course.client.view.html',
      controller: 'CoursesSettingController',
      controllerAs: 'vm',
      resolve: {
      courseResolve: getCourse
      },
      data: {
      pageTitle: 'Course {{ courseResolve.name }} Setting'
      }
      })
      .state('courseSetting.objective', {
      url: '/objective',
      templateUrl: '/modules/courses/client/views/course-setting/course-objective.setting.course.client.view.html'
      })
      .state('courseSetting.lesson', {
      url: '/lesson',
      templateUrl: '/modules/courses/client/views/course-setting/lesson-plan.setting.course.client.view.html'
      })
      .state('courseSetting.content', {
      url: '/content',
      templateUrl: '/modules/courses/client/views/course-setting/course-content.setting.course.client.view.html'
      })
      .state('courseSetting.homepage', {
      url: '/homepage',
      templateUrl: '/modules/courses/client/views/course-setting/course-homepage.setting.course.client.view.html'
      })
      .state('courseSetting.setting', {
      url: '/setting',
      templateUrl: '/modules/courses/client/views/course-setting/course-setting.setting.course.client.view.html'
      })
      .state('courseSetting.preview', {
      url: '/preview',
      templateUrl: '/modules/courses/client/views/course-setting/course-preview.setting.course.client.view.html'
      })
      .state('courseSetting.help', {
      url: '/help',
      templateUrl: '/modules/courses/client/views/course-setting/help.setting.course.client.view.html'
      });
  }

  getCourse.$inject = ['$stateParams', 'CoursesService'];

  function getCourse($stateParams, CoursesService) {
    return CoursesService.get({
      courseId: $stateParams.courseId
    }).$promise;
  }

  newCourse.$inject = ['CoursesService'];

  function newCourse(CoursesService) {
    return new CoursesService();
  }
  newStudent.$inject = ['$stateParams','StudentService'];

  function newStudent($stateParams, StudentService) {
    return new StudentService({courseId: $stateParams.courseId});
  }

}());



// class create state

// .state('courses.create', {
//         url: '/create',
//         templateUrl: 'modules/courses/client/views/form-course.client.view.html',
//         controller: 'CoursesController',
//         controllerAs: 'vm',
//         resolve: {
//           courseResolve: newCourse
//         },
//         data: {
//           pageTitle: 'Courses Create'
//         }
//       })

//course view

// .state('courses.view', {
//         url: '/:courseId',
//         templateUrl: '/modules/courses/client/views/view-course.client.view.html',
//         controller: 'CoursesController',
//         controllerAs: 'vm',
//         resolve: {
//           courseResolve: getCourse
//         },
//         data: {
//           pageTitle: 'Course {{ courseResolve.name }}'
//         }
//       })